# NESTJS BOILER PLATE - example with REST API

## Description

NestJS boilerplate with examples of REST API + JWT authentication with passport. Using postgreSQL with typeORM.
Typescript.

## Installation

```bash

$ git clone git@bitbucket.org:ivanvukusic15/nestjs-boilerplate.git

$ cd nestjs-boilerplate

$ yarn install

# create and start new docker container for postgreSQL
$ docker-compose up -d
```

## Running the app

```bash
# development
$ yarn start

# watch mode
$ yarn start:dev

# production mode
$ yarn start:prod
```

## Test

```bash
# unit tests
$ yarn test

# e2e tests
$ yarn test:e2e

# test coverage
$ yarn test:cov
```

## Stay in touch

- Author - [Ivan Vukušić](ivanvukusic15@gmail.com)
- Website - [https://nordit.co/](https://nordit.co/)
